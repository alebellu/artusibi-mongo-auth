/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'mongodb', 'artusi-kitchen-tools'], function($, mongodb, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var mongoAuthType = function(context) {
        var auth = this;
        auth.context = context;
    };

    /**
     * @options:
     *   @authIRI the IRI of the auth.
     */
    mongoAuthType.prototype.init = function() {
        var dr = $.Deferred();
        var auth = this;

        // copy mongodb specific options into easy typing options.
        auth.conf.host = auth.conf["ama:host"];
        auth.conf.port = auth.conf["ama:port"];
        auth.conf.user = auth.conf["ama:user"];
        auth.conf.pwd = auth.conf["ama:pwd"];
        auth.conf.db = auth.conf["ama:db"];

        auth.conf.loginAtStartup = auth.conf["ama:loginAtStartup"];
        if (auth.conf.loginAtStartup === undefined) {
            auth.conf.loginAtStartup = true;
        }

        if (auth.conf.loginAtStartup) {
            auth.login().done(drdf(dr)).fail(drff(dr));
        }
        else {
            dr.resolve();
        }

        return dr.promise();
    };

    mongoAuthType.prototype.getAuthority = function(options) {
        return "http://www.mongodb.org/";
    };

    mongoAuthType.prototype.getIcon32Url = function() {
        return "http://github.drawer.artusi.ssoup.org/1.0/images/github_white_black_cat_32.png";
    };

    mongoAuthType.prototype.login = function(options) {
        var dr = $.Deferred();
        var auth = this;
        var kitchen = auth.context.kitchen;

        var MongoClient = mongodb.MongoClient;
        MongoClient.connect("mongodb://" + auth.conf.user + ":" + auth.conf.pwd + "@" + auth.conf.host + ":" + auth.conf.port + "/" + auth.conf.db, function(err, db) {
            if(err) { dr.reject(); return console.dir(err); }

            var authInfo = {
                user: auth.conf.user,
                specificAuthInfo: {
                    db: db
                }
            };

            kitchen.shelf.authInfo[auth.getIRI()] = authInfo;
            dr.resolve(authInfo);
        });

        return dr.promise();
    };

    mongoAuthType.prototype.logout = function(options) {
        var dr = $.Deferred();
        var auth = this;
        var kitchen = auth.context.kitchen;

        kitchen.shelf.authInfo[auth.getIRI()]["specificAuthInfo"].db.close();
        delete kitchen.shelf.authInfo[auth.getIRI()];

        dr.resolve();

        return dr.promise();
    };

    return mongoAuthType;
});
